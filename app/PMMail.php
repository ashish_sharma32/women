<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMMail extends Model
{

	protected $table = 'p_m_mails';
	
    protected $fillable = [
        'name', 'ip', 'email' ,'number'
    ];
}
