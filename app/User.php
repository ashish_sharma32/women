<?php

namespace App;
use DB;
use Request;
use Validator;
use Auth;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['name','email','telephone','signature'];
}
