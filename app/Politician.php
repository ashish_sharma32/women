<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Politician extends Model
{
    protected $fillable = [
        'name', 'email','party','constituency','opinion',
    ];

    protected $hidden = ['created_at','updated_at'];

    public function state()
    {
        return $this->belongsTo('App\State');
    }

}
