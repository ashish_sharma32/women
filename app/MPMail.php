<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MPMail extends Model
{
    protected $fillable = [
        'id', 'mail_to',
    ];
}
