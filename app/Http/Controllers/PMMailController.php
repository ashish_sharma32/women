<?php

namespace App\Http\Controllers;

use App\PMMail;
use App\MPMail;
use Mail;
use Redirect;
use Illuminate\Http\Request;

class PMMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PMMail  $pMMail
     * @return \Illuminate\Http\Response
     */
    public function show(PMMail $pMMail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PMMail  $pMMail
     * @return \Illuminate\Http\Response
     */
    public function edit(PMMail $pMMail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PMMail  $pMMail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PMMail $pMMail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PMMail  $pMMail
     * @return \Illuminate\Http\Response
     */
    public function destroy(PMMail $pMMail)
    {
        //
    }


    public function sendMailPM($name,$email,$number){
        
        $uemail = $email;
        $ip = \Request::ip();
        $data = array(
        'name' => $name,
        'ip' => $ip,
        );

        $emails = ["pmh7rcr@gov.in","connect@mygov.nic.in"];

        $subject = $name .' Requests for Introduction and Implementation of Women’s Reservation Bill';
        foreach($emails as $email)
        {
            try {
            Mail::send('emails.mailtopm', $data, function ($message) use($email,$ccmail,$subject) {
                $message->from('womenfor33@aimc.in', 'Women For 33% - All India Mahila Congress');
                $message->to($email)->subject($subject);});

        } catch (Exception $e) {
            return "error";
        }

        PMMail::create(['name' => $name,'ip'=> $ip, 'number'=>$number, 'email' => $uemail]);
            
        }
        
        return "success";
    }
}
