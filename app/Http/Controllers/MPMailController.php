<?php

namespace App\Http\Controllers;

use App\MPMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MPMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MPMail  $mPMail
     * @return \Illuminate\Http\Response
     */
    public function show(MPMail $mPMail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MPMail  $mPMail
     * @return \Illuminate\Http\Response
     */
    public function edit(MPMail $mPMail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MPMail  $mPMail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MPMail $mPMail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MPMail  $mPMail
     * @return \Illuminate\Http\Response
     */
    public function destroy(MPMail $mPMail)
    {
        //
    }
}
