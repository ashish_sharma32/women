<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MPMail;
use App\User;

class PageController extends Controller
{
    public function home(){
    	$user_count = User::all();
    	$user_count = $user_count->count();
    	$mail_count = MPMail::all();
    	$mail_count = $mail_count->count();
    	return view('home')->with(['mail_count'=>$mail_count,'user_count'=>$user_count]);
    }
    public function giveYourSupport(){
    	return view('give-your-support');
    }
    public function thanksmp(){
    	return view('thanks-mp');
    }
    public function thanks(){
    	return view('thanks');
    }
    public function blog1(){
    	return view('blog1');
    }
    public function blog2(){
        return view('blog2');
    }
}
