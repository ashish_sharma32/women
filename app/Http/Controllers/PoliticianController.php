<?php

namespace App\Http\Controllers;

use App\Politician;
use App\MPMail;
use Mail;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Controllers\Controller;

class PoliticianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $politician = Politician::all();
        return $politician;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Politician  $politician
     * @return \Illuminate\Http\Response
     */
    public function show(Politician $politician)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Politician  $politician
     * @return \Illuminate\Http\Response
     */
    public function edit(Politician $politician)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Politician  $politician
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Politician $politician)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Politician  $politician
     * @return \Illuminate\Http\Response
     */
    public function destroy(Politician $politician)
    {
        //
    }

    public function sendMailMP(){
        
        // $politician = Politician::find($id);
        // $name = $politician->name;
        // $email = $politician->email;
        // $token = $politician->token;
        
        $name = "Ashish Sharma";

        $data = array(
        'name' => $name
        );

        $email = "sharma.asmith7@gmail.com";

        try {
            Mail::send('emails.mailtopm', $data, function ($message) use($email) {
                $message->from('womenfor33@aimc.in', 'All India Mahila Congress');
                $message->to($email)->subject('Request for introduction and implementation of Women’s Reservation Bill');});   
        } catch (Exception $e) {
            return "error";
        }
        $newmail = new MPMail;
        $newmail->mail_to = $email;
        $newmail->save();
        return "success";
    }

    public function sendMailPM($name){
        
        $data = array(
        'name' => $name,
        'ip' => \Request::ip(),
        );

        $email = "sharma.asmith7@gmail.com";

        $ccmail = "rajndwines@gmail.com";

        try {
            Mail::send('emails.mailtopm', $data, function ($message) use($email,$ccmail) {
                $message->from('womenfor33@aimc.in', 'Women For 33% - All India Mahila Congress');
                $message->to($email)->cc($ccmail)->subject('Request for introduction and implementation of Women’s Reservation Bill');});   
        } catch (Exception $e) {
            return "error";
        }
        $newmail = new MPMail;
        $newmail->mail_to = $email;
        $newmail->save();
        return "success";
    }


    public function sendMailAll(){
        
        $emails = Politician::pluck('email');
        $status = "Yes";
        $response = array();
        foreach($emails as $email){
            try {
                $name = Politician::where('email',$email)->value('name');
                $token = Politician::where('email',$email)->value('token'); 
                $data = array(
                        'name' => $name,
                        'token' => $token,
                        );
                Mail::send('emails.opinion', $data, function ($message) use($email) {
                $message->from('president@aimc.in', 'All India Mahila Congress');
                $message->to($email)->subject('#WomenFor33%');});
            } catch(\Exception $e){
                $status = 'No';
                dd($e);
            }
            $response[$email]=$status;
        }
        
        return $response;
    }

    public function setOpinion($token,$opinion){

        $politician = Politician::where('token', $token)->first();

        if (!$politician)
        {
            return "Error";
        }

        $politician->opinion = $opinion;

        $politician->save();

        $data = array(
                    'name' => $politician->name,
                    'email' => $politician->email,
                    'opinion'=> $politician->opinion
                );

        Mail::send('emails.response', $data, function ($message) {
                $message->from('president@aimc.in', 'All India Mahila Congress');
                $message->to('connect@aimc.in')->subject("Response From MP's E-mail");});

        return redirect('/thanks-mp');
    }

    public function getConstituency($id){
        $constituencies = Politician::where('state_id',$id)->pluck('id','constituency');
        return $constituencies;
    }

    public function getOpinion($id){
        $politician = Politician::find($id);
        return $politician;
    }



}
