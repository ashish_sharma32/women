/*------------------------------------------------------------------
 [Table of contents]

 - Video
 - Youtube
 - Add .html file on page
 - Menu-Bar
 - Counter
 - Homepage Carousel
 - Owl Carousel
 - HeatMap

 -------------------------------------------------------------------*/

"use strict";

// ======================= Video =============================
var iframeVideo = document.getElementById("iframeVideo");
if (iframeVideo) {
    document.querySelector(".video-overlay").onclick = function () {
        this.style.display = "none";
        iframeVideo.setAttribute('src',
            'https://player.vimeo.com/video/10260175?title=0&amp;byline=0&amp;portrait=0?rel=0&amp;autoplay=1');
    };
}

// ====================== Youtube ============================
var iframeYoutube = document.getElementById("iframeYoutube");
if (iframeYoutube) {
    document.querySelector(".youtube-overlay").onclick = function () {
        this.style.display = "none";
        iframeYoutube.setAttribute('src',
            'https://www.youtube.com/embed/5pjf_mmdZpE');
    };
}

$(function() {
    
    // ===================== Menu-Bar ======================
    $(document).on('click','button.navbar-toggle', function(){
        if($(".slider-info")) {
            $(".slider-info").each(function () {
                $(this).toggleClass("displayNone");
            });
            $(".slider").toggleClass("shadowButton");
        }
        if($(".banner h1")) {
            $(".banner h1").toggleClass("displayNone");
            $(".banner ul").toggleClass("displayNone");
            $(".banner").toggleClass("shadowButton");
        }
        if($(".page-404")) {
            $(".page-404 .menuBar").css("background","rgb(06, 06, 06)");
        }
        if($(".page-single")) {
            $(".page-single .menuBar").css("background","rgb(06, 06, 06)");
        }
    });

    $(document).on('mouseover',"li.dropdown", function(){
        $(this).addClass("open");
    }).on('mouseout',"li.dropdown", function(){
        $(this).removeClass("open");
    });

    // ===================== Counter ======================
    var countbox = $("#counts");
    if (countbox.length>0) {
        var show = true;
        $(window).on("scroll load resize", function () {

            if (!show) return false;

            var w_top = $(window).scrollTop();
            var e_top = countbox.offset().top;
            var w_height = $(window).height();
            var d_height = $(document).height();
            var e_height = countbox.outerHeight();

            if (w_top + 900 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
                $(".spincrement").spincrement({
                    thousandSeparator: "",
                    duration: 3000
                });
                show = false;
            }
        });
    }

    // =================== Homepage Carousel ====================
    var slider=$('.slider');
    if(slider.length>0) {
        $("#home-carousel .slider-info h4").addClass('animated fadeInDown');
        $("#home-carousel .slider-info h2").addClass('animated fadeInDown');
        $("#home-carousel .slider-info .buttons").addClass('animated fadeInDown');
    }
    $('#home-carousel').carousel(
        {
            interval: 7000
        }
    );

    // ===================== Owl Carousel ======================
    var owl = $('.owl-carousel');
    if (owl.length>0) {
        owl.owlCarousel({
            autoplayHoverPause: true,
            autoplayTimeout: 4000,
            autoplay: true,
            loop: false,
            margin: 30,
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                700: {
                    items: 3
                },
                1000: {
                    items: 3
                }
            }
        });
    }

});
// ======================= HeatMap =============================

$(function(){
    d3.select(window).on("resize", sizeChange);
        //Set d3 scale
        var color_domain = ['GEN','SC','ST'];
        var legend_labels = ['GEN','SC','ST'];
        var color = d3.scale.threshold()
          .domain(color_domain)
          .range(["#2552AC","#2552AC", "#2552AC","#2552AC"]);
        //Set tooltip
        var div = d3.select("body").append("div")
         .attr("class", "tooltip")
         .style("opacity", 0);
        //Set d3 projection, path and svg
        var projection = d3.geo.mercator()
            .center([78, 27])
            .scale(1200);
        var path = d3.geo.path()
            .projection(projection);
        var svg = d3.select("#pc_map")
          .append("svg")
          .attr("width", "100%")
          .attr("height","50%")
          .append("g");
        //Wait for data files to download before drawing
        queue()
          .defer(d3.json, "/india_pc_2014_simplified.topojson")
          .defer(d3.json, "/india_state_2014_simplified.topojson")
          .defer(d3.csv, "/results.csv") //sample data
          .await(ready);
        function ready(error, pc, state, data) {
          //Set up for visualizing sample data
          var pairResultWithId = {};
          var pairNameWithId = {};
          var pairMPNameWithID = {};
          data.forEach(function(d) {
            pairResultWithId[d.ST_CODE + d.PC_CODE] = d.Res;
            pairNameWithId[d.ST_CODE + d.PC_CODE] = d.PC_NAME;
            pairMPNameWithID[d.ST_CODE + d.PC_CODE] = d.PC_MP;
          });
          //Drawing pc boundaries
          var pc_geojson = topojson.feature(pc, pc.objects.india_pc_2014);
          svg.selectAll(".pc")
              .data(pc_geojson.features)
              .enter().append("path")
              .attr("class", "pc")
              .attr("d", path)
              .style ( "fill" , function (d) {
                var result = pairResultWithId[d.properties.ST_CODE + d.properties.PC_CODE];
                if (result!='') {
                    if(result=='YES'){
                        var col = "#53ac4b";
                        return col;
                    } 
                     if(result=='NO'){
                        var col = "#ff0700";
                        return col;
                    } 
                    
                    return color(result);}
                
              })
              .style("opacity", 1)
          ;
          //Drawing state boundaries          
          var state_geojson = topojson.feature(state, state.objects.india_state_2014);
          svg.selectAll(".state")
              .data(state_geojson.features)
              .enter().append("path")
              .attr("class", "state")
              .attr("d", path);
        }
        //Set up for drawing html legend elements
        var legend = d3.select('.legend-scale')
          .append('ul')
          .attr('class', 'legend-labels');
        var keys = legend.selectAll('li')
          .data(color_domain);
        keys.enter().append('li')
          .text(function(d, i){ return legend_labels[i];})
          .append('span')
          .style('background', function(d) { return color(d); })
          ;
        //Function called when window is resized
        
        
        function sizeChange() {
          d3.select("g").attr("transform", "scale(" + jQuery("#pc_map").width()/1000 + ")");
          jQuery("svg").height(jQuery("#pc_map").width()*0.7);
        }
        
        window.onload = sizeChange();
})

// ======================= FlexSelect =============================

$(function() {
   $("select.flexselect").flexselect(); 
});
