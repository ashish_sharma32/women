<meta charset="UTF-8">
<title> @yield('title') </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<link rel="shortcut icon" href="/images/favicon.ico">
<!-- Open Graph -->
<meta property="og:url" content="http://www.womenfor33.aimc.in" />
<meta property="og:type" content="website" />
<meta property="og:title" content="#WomenFor33 - All India Mahila Congress Campaign" />
<meta property="og:description" content="Join our initiative requesting Prime Minister of India to truly ensure Gender empowerment and Parity by bringing in Women's Reservation Bill." />
<meta property="og:image" content="http://womenfor33.aimc.in/images/modi-stalls-women-2.jpg" />


<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="http://www.womenfor33.aimc.in">
<meta name="twitter:title" content="#WomenFor33 - All India Mahila Congress Campaign">
<meta name="twitter:description" content="Join our initiative requesting Prime Minister of India to truly ensure Gender empowerment and Parity by bringing in Women's Reservation Bill.">
<meta name="twitter:creator" content="@mahilacongress">
<meta name="twitter:image" content="http://womenfor33.aimc.in/images/modi-stalls-women-2.jpg">