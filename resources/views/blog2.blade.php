@extends('layout')
@section('title') Bridging Gender Gap in Education Through ICT by Sharmistha Mukherjee  - #WomenFor33 | All India Mahila Congress @stop 
@section('page-content')
<div class="banner">
    <div class="shadow-main">
    </div>
</div>
<div class="container ">

    <div class="title-single">
        <h1>Bridging Gender Gap in Education Through ICT</h1>
        <ul class="breadcrumb breadcrumb-single">
            <li><a href="/">Home</a></li>
            <li><a href="/">Bridging Gender Gap in Education Through ICT </a></li>
        </ul>
    </div>

    <div class="row">
        <div class="col-lg-8 col-md-7 col-sm-6">
            <div class="main-post">

                <div class="post-single">
                    <div class="person-card post-single-cont">
                        <div class="person-img">
                            <a href="/" class="img-link">
                                <img src="../images/bridging-gender-gap-in-education.jpg" alt="medium-2">
                            </a>
                        </div>
                        <div class="person-content">
                            <ul class="gb-list">
                                <li>
                                    <a href="/"> May 17, 2017 </a>
                                </li>
                                <li>
                                    <a href="#author"> by Sharmistha Mukherjee</a>
                                </li>
                            </ul>
                            <p>Gender disparity is a burning issue in India. Indian Constitution not only guarantees gender equality, but also directs the State to take positive action in this direction. Despite the multi-dimensional legislative and other powerful policy initiatives aimed at gender parity, India suffers from gross gender inequity in literacy levels, labour force participation and representation in administrative and political institutions.</p>
                            <p>Education is a vehicle to develop human capital and is one of the most effective tools for empowering the marginalised. The Right to Education Act makes it mandatory for the State to provide elementary education to all children, but what is lacking is the focus on gender-specific investments in education. The literacy rate of women has increased from 53.7% in 2001 to 65.5% in 2011. However, what is disturbing is that it lags behind male literacy by 16.7%. To address the inequality and to lend true meaning to empowerment, policy interventions are required to facilitate women to expand their freedom of choice. An important tool to expand this choice is education. Positive and innovative intervention in education for women and girl child is required to make a positive difference in their lives.</p>
                            <p>A major cause for the continuing gender disparity in education is the negative mind-set still prevalent among a large section section of our society. Women are subjected to restricted mobility, lesser access to education and health facilities, and lower decision-making power. Preference for male child and prioritising their needs continue to persist. If investment in education is divorced from this unfortunate reality, the initiatives are bound to suffer. It is, therefore, not enough to build physical infrastructure in terms of school buildings and providing teachers. There is a need to address the root causes of discrimination against women in order to make a decisive impact on national educational attainments. In the socio-economic backdrop of a developing country like India, ICT could be an effective means of delivering education to women suffering from many disadvantages. ICT could be used innovatively to bridge the gender gap in education. The greatest advantage of ICT is that it is not confined to physical space of an educational institution, and can provide flexibility of time.</p>
                            <p>However women may face potential barriers even in using ICT tools. Lack of access to suitable technology, high cost and skill deficiency compounded with patriarchal mindset, financial dependence on men, and restricted mobility could be major impediments for gender application of ICT in education. Even when there is gender equality in enrolment and access, discouragement to girls from choosing subjects like science and technology at the secondary and higher levels could deny them equitable opportunities. If these issues are not addressed, simply introducing ICT as a tool of learning would create a digital gender disparity.</p>
                            <p>Successful gender-based ICT interventions would require community approach. The entire community should feel benefitted by use of wide-ranging ICT applications in their lives. This would help build a positive outlook about ICT, generate demand and develop community support for using ICT to educate and empower women and girls. A sustained program to build awareness within communities on the advantages of ICT and development of specific programs on settling day-to-day problems is required. At the same time, ICT service providers have to gauge and understand the ICT experiences of women, their technology needs and their desired mode of usage. Since in traditional societies, it is generally boys who are encouraged to avail of ICT tools in education and knowledge pursuit activities, girls should be brought into the fold of ICT based education through differential attention.</p>
                            <p>Women are equal to men in their potential contribution to the economy. However, women are often under-represented in the country’s productive efforts. This is detrimental to the cause of national progress. Women represent a huge reservoir of talent. It is in the interest of the nation that this reservoir is tapped and directed effectively towards the cause of nation building.</p>
                            </p>

                            <div class="blockquote-cont blue">
                                <div class="quotes">
                                    <span>“ </span>
                                    <blockquote>
                                        <p>To address the inequality and to lend true meaning to empowerment, policy interventions are required to facilitate women to expand their freedom of choice. An important tool to expand this choice is education. </p>
                                    </blockquote>
                                </div>
                            </div>

                            <div class="tags-title">

                                <div class="social-link">
                                    <ul>
                                        <li><a href="https://www.tumblr.com/" class="circle c-1"><i class="icon-tumblr "></i></a></li>
                                        <li><a href="https://www.facebook.com/fruitfulc0de" class="circle c-2"><i class="icon-facebook "></i></a></li>
                                        <li><a href="https://www.pinterest.com/fruitfulcode/" class="circle c-3"><i class="icon-pinterest "></i></a></li>
                                        <li><a href="https://vimeo.com/" class="circle c-4"><i class="icon-vimeo "></i></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="author" class="blue">
                        <div class="about-author">
                            <div class="author">
                                <img src="../images/priyanka_chaturvedi.jpeg" alt="author">
                                <h3 class="">Sharmistha Mukherjee</h3>
                                <p>National Medi Panelist INC, Cheif Spokesperson and Incharge Communication DPCC</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-5 col-sm-6 right-post-single">
            <div id="sidebar">
                <div class="sidebar blue ">

                    <div class="search">
                        <form class="form-search" action="//google.com.ua/search" target="_blank" method="get" onsubmit="return q.value!=''" role="search">
                            <label>Search</label>
                            <input type="search" name="q" placeholder="Search...">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                  <!--   <div class="recent-posts mb">
                        <h4> Recent Posts </h4>
                        <div class="news-1">
                            <a href="">
                                <img src="images/bridging-gender-gap-in-education.jpg" alt="...">
                            </a>
                            <div class="news">
                                <h5 class="news-h"><a href="https://thx-html.fruitfulcode.com/charity-blog-single">Blog Title</a> </h5>
                                <p class="date">June 28, 2017</p>
                            </div>
                        </div>
                    </div> -->

                </div>
            </div>
        </div>

    </div>
</div>
@stop