@extends('layout')
@section('title') Give Your Support @stop
@section('page-content')
<div class="banner">

<div class="shadow-main">
    <h1> Give Your Support </h1>
</div>

</div>
<div class="main-contact">
    <div class="container">
    	<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="text-center">
                    <h2 class="title-text">Be a part of our Initiative</h2>
                    <div class="wave-line wave-center"></div>
                    <p class="paragraph-white ">We are collecting digital signatures all around the country to show support for our Campaign.</p>
                    <p class="paragraph-white ">Become a part of our initiative by filling out the form below : </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="leave-reply">
                	{!! Form::open(['route' => 'user.store']) !!}
                    	<div class="form-group">
                        	<input id="name" name="name" type="text" placeholder="Name" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <input id="email" name="email" type="email" placeholder="E-mail" class="form-control">
                        </div>
                        <div class="form-group">
                        	<input id="tel" name="telephone" type="tel" placeholder="Telephone" class="form-control">
                        </div>
                        <div class="form-group">
                              	<canvas id="signature-pad" class="signature-pad" width=500 height=200></canvas>
                              	<button id="clear">Clear</button>
                            </div>
                        <input type="hidden" name="signature">
                        <button id="sub-btn" class="red-btn red-btn-form">Submit</button>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('page-scripts')
<script type="text/javascript">
$spinner = "<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>";
var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
		penColor: 'rgb(0, 0, 0)'
	});
$("#clear").click(function(event) {
	event.preventDefault();
	signaturePad.clear();
});

$("#sub-btn").click(function(event) {
	$ele = $(this);
    $ele.html($spinner);
	event.preventDefault();
	var path = signaturePad.toDataURL();
	$("input[name='signature']").val(path);
	var name = $("input[name='name']").val();
	var email = $("input[name='email']").val();
	var telephone = $("input[name='telephone']").val();
	var signature = $("input[name='signature']").val();
	$.ajax({
    type: "post",
    url: "/user",
    data: {
            'name': name,
            'email': email,
            'telephone': telephone,
            'signature': signature,
            '_token': '{{csrf_token()}}'
        },
    success: function(data){          
    	if(data=="success"){
    		window.location.href = "/thanks";
    	}   
    }
  });


});
</script>
@stop