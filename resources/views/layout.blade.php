<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.meta')
    @include('partials.styles')
    @include('partials.polyfills')
    </head>
<body>
<div class="page">
    <div id="menuBar-charity"><div class="menuBar">
    <nav class="navbar navbar-default ">
        <div class="container p0">
            <div class="navbar-header">
                <div class="logo">
                    <a href="#" class="navbar-brand">
                        <!-- <img src="https://thx-html.fruitfulcode.com/wp-content/themes/thx-html/assets/images/logo.png" alt="Homepage logo"> -->
                      <!--   <span>#womenfor33</span> -->
                      <h2 style="color:#fff">AIMC Campaign</h2>
                    </a>
                </div>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
            </div>
            <div class="navbar-right">

                <div id="navbar-menu" class="collapse navbar-collapse ">
                    <ul class="nav navbar-nav" data-in="fadeInDown" data-out="fadeOutUp">
                        <li>
                            <a href="/" aria-expanded="false" title="Home">HOME</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>

</div>
    @yield('page-content')
    <div id="footer"><footer class="blue">
    <div class="footer-container">
        <div class="container">
            <div class="row">
                <div class="footer-content">

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb">
                        <div class="footer-about-info">
                            <h4> About Our Campaign </h4>
                            <p class="justify">Our current campaign #WomenFor33% is a landmark endeavor to gather public support for giving women 33% reservation in all elected bodies from Panchayat to Parliament.</p>
                            <ul>
                                <li><i class="icon-location-1"></i>
                                    <span>24, Akbar Road, New Delhi 110011</span></li>
                                <li><i class="icon-phone"></i>
                                    <span>Phone: 91-11-23014659 </span></li>
                                <li><i class="icon-mail"></i>
                                    <span>Email: <a href="mailto:#">connect@aimc.in</a></span></li>
                            </ul>
                        </div>
                        <div class="social-link footer-social-link">
                            <ul>
                                <li><a href="https://www.facebook.com/mahilacongress" class="circle c-2"><i class="icon-facebook "></i></a></li>
                            </ul>

                             
                        </div> <!-- footer-social-link -->
                    </div>
<!-- 
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 mb">
                        <h4> Contact Us </h4>
                        <form action="...">
                            <div class="form-group">
                                <input id="first_name" name="first_name" type="text" placeholder="Name" class="form-control" required="">
                            </div>
                            <div class="form-group">
                                <input id="email" name="email" type="email" placeholder="E-mail" class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="4" placeholder="Message..."></textarea>
                            </div>
                            <div class="form-group buttons ">
                                <input type="submit" class="red-btn red-btn-form" value="Send Message">
                            </div>
                        </form>
                    </div>
 -->
                </div>
            </div>
        </div>
    </div>

    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p class="copy-title">Copyright © 2017 | Women For 33% - All India Mahila Congress
                    </p>
                </div>
            </div>
        </div>
    </div>

</footer></div>
</div>  
@include('partials.scripts')
@yield('page-scripts')
</body>
</html>