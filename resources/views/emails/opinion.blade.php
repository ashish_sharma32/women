<html class="gr__localhost"><head>
    <title>Women Reservation Bill</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        
        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        /* Prevent WebKit and Windows mobile changing default text sizes */
        
        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        /* Remove spacing between tables in Outlook 2007 and up */
        
        img {
            -ms-interpolation-mode: bicubic;
        }
        /* Allow smoother rendering of resized image in Internet Explorer */
        /* RESET STYLES */
        
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }
        
        table {
            border-collapse: collapse !important;
        }
        
        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }
        /* iOS BLUE LINKS */
        
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
        /* MOBILE STYLES */
        
        @media  screen and (max-width: 525px) {
            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }
            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            .logo img {
                margin: 0 auto !important;
            }
            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
            .mobile-hide {
                display: none !important;
            }
            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }
            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }
            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
            .padding {
                padding: 10px 5% 15px 5% !important;
            }
            .padding-meta {
                padding: 30px 5% 0px 5% !important;
                text-align: center;
            }
            .no-padding {
                padding: 0 !important;
            }
            .section-padding {
                padding: 50px 15px 50px 15px !important;
            }
            /* ADJUST BUTTONS ON MOBILE */
            .mobile-button-container {
                margin: 0 auto;
                width: 100% !important;
            }
            .mobile-button {
                padding: 15px !important;
                border: 0 !important;
                font-size: 16px !important;
                display: block !important;
            }
        }
        /* ANDROID CENTER FIX */
        
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>

<body style="margin: 0 !important; padding: 0 !important;" data-gr-c-s-loaded="true">

    <!-- HIDDEN PREHEADER TEXT -->
    <div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
        Provide Support For #WomenFor33%
    </div>

    <!-- HEADER -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody><tr>
            <td bgcolor="#ffffff" align="center">
                <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
                    <tbody><tr>
                        <td align="center" valign="top" style="padding: 15px 0;" class="logo">
                            <a href="http://litmus.com" target="_blank">
                                <img alt="" src="http://aimc.in/wp-content/uploads/2017/08/logo-aimc.jpg" height="60" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
                            </a>
                        </td>
                    </tr>
                </tbody></table>
                <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff" align="center" style="padding: 40px 15px 70px 15px;" class="section-padding">
                <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
                    <tbody><tr>
                        <td>
                            <!-- HERO IMAGE -->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody><tr>
                                    <td class="padding" align="center">
                                        <a href="http://litmus.com" target="_blank">
                                            <img src="http://aimc.in/wp-content/uploads/2017/10/women-reservation.jpg" width="500" height="400" border="0" alt="" style="display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;" class="img-max">
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <!-- COPY -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                <td align="center" style="font-size: 23px;font-family: Helvetica, Arial, sans-serif;color: #333333;padding-top: 50px;" class="padding">Do You Support #WomenFor33% ?</td>
                                            </tr>
                                            <tr>
                                                <td align="justify" style="padding: 50px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding">Dear, {!! $name !!}<br><br>I am writing to you today on a matter that concerns each of us deeply in our role
as representatives of the people and citizens of this great nation.<br>As you know, women still remain poorly represented in policy making and legislative bodies in our country. In fact they stand at a shamefully poor&nbsp;11% representation in Parliament after the last general
election.<br><br>As India moves ahead on its journey of becoming a leading Demovracy in world,
there will be many issues that Parliament or Legislative Assemblies will vote on
that will disproportionately affect women: labour rights, minimum wages,
security and privacy, just to name a few. I strongly believe that when these issues
come up, the debates in the Legislative Houses will be best served if there are
more female voices that will bring their own unique experiences and perspectives
to ensure that we have more balanced legislative outcomes.<br><br><strong>As a fellow Member of Parliament, I seek your support in asking the government that it introduces the Bill in the upcoming Winter&nbsp;Session of Parliament 2017, so that it is not only passed but also implemented before the&nbsp;2019 General&nbsp;and&nbsp;Assembly elections.</strong><br><br>Not only your voters,&nbsp;but the world will be watching&nbsp;the decisions you take today
to&nbsp;empower&nbsp;8% of humanity!<br><br><strong>I would request you to begin with clicking on the relevant button below to indicate your views on the Bill. Your vote will be a part of a map that will be visible to our fellow citizens so that they know each of our views on the issue</strong><br><br>Thank you for your time.<br><br>Sushmita Dev, MP<br>President,<br>All India Mahila Congress</td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <!-- BULLETPROOF BUTTON -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
<tr>
                                                <td align="center" style="padding-top: 25px;" class="padding">
                                                    <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                                        <tbody><tr>
                                                            

<td align="center" style="border-radius: 3px;" bgcolor="#5cb85c"><a href="{{url('/')}}/setopinion/{!! $token !!}/YES" target="_blank" style="width:200px;font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; border-radius: 3px; padding: 15px 25px; border: 1px solid #5cb85c; display: inline-block;" class="mobile-button">Yes</a></td>


                                                        </tr>
                                                    </tbody></table>
                                                </td>
                                            </tr><tr>
                                                <td align="center" style="padding-top: 25px;" class="padding">
                                                    <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                                        <tbody><tr>
                                                            

<td align="center" style="border-radius: 3px;" bgcolor="#E75151"><a href="{{url('/')}}/setopinion/{!! $token !!}/NO" target="_blank" style=" width:200px;font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; border-radius: 3px; padding: 15px 25px; border: 1px solid #E75151; display: inline-block;" class="mobile-button">No</a></td>


                                                        </tr>
                                                    </tbody></table>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
            </td>
        </tr>
        
        
        <tr>
            <td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">
                <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
                <!-- UNSUBSCRIBE COPY -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
                    <tbody><tr>
                        <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">24, Akbar Road, South Block, Man Singh Road Area, New Delhi 110011<br>
                            
                            
                            <a href="http://litmus.com" target="_blank" style="color: #666666; text-decoration: none;">View this email in your browser</a>
                        </td>
                    </tr>
                </tbody></table>
                <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
            </td>
        </tr>
    </tbody></table>
</body></html>
