@extends('layout')
@section('title') Stride Towards Equality by Priyanka Chaturvedi - #WomenFor33 | All India Mahila Congress @stop 
@section('page-content')
<div class="banner">
    <div class="shadow-main">
    </div>
</div>
<div class="container ">

    <div class="title-single">
        <h1> Stride Towards Equality </h1>
        <ul class="breadcrumb breadcrumb-single">
            <li><a href="/">Home</a></li>
            <li><a href="/">Stride Towards Equality</a></li>
        </ul>
    </div>

    <div class="row">
        <div class="col-lg-8 col-md-7 col-sm-6">
            <div class="main-post">

                <div class="post-single">
                    <div class="person-card post-single-cont">
                        <div class="person-img">
                            <a href="/" class="img-link">
                                <img src="../images/blog2.jpg" alt="medium-2">
                            </a>
                        </div>
                        <div class="person-content">
                            <ul class="gb-list">
                                <li>
                                    <a href="/"> May 17, 2017 </a>
                                </li>
                                <li>
                                    <a href="#author"> by Priyanka Chaturvedi</a>
                                </li>
                            </ul>
                            <p>The women across the world have struggled over the years to be able to get equal opportunities and access for themselves. Who can forget the suffragette movement seeking voting rights denied to women in the UK? Even in the USA women got voting rights much after men. For India the journey for all post independence and after we accepted the constitution was with the intent of equality for all. Dr. BR Ambedkar the architect of the constitution believed that the true development of a society can truly be measured by the degree of success women have achieved. Somewhere down the line this mission and spirit with which we began as an independent, republic nation has been lost.</p>

                            <p>To deny no progress has been made towards empowering women would be undermining all the efforts undertaken through policy interventions as well as the sheer determination of India’s women to succeed. There are many inspiring women who have set an example of being successful despite all odds. However is that enough? If we look at the participation of women in economic development of the nation their role is not reflective of the near 50% of the population that we are.</p>

                            <p>Last year, the World Economic Forum’s annual Global Gender Gap Report ranked India 87 in terms of gender equality in economy, education, health, and political representation. Women’s declining labour participation, under-representation in Parliament, skewed child sex ratio, and prevalent gender-based violence are recognised challenges.</p>

                            <p>The National Sample Survey (NSS) data for India shows that labour force participation rates of women aged 25-54 have stagnated at about 26-28% in urban areas, and fallen substantially from 57% to 44% in rural areas. A declining participation in labour workforce is also costing India financially, a 2015 report by the Mckinsey Global Institute shows how if more women in India joined the labour force, it would add about 1.4 percentage points to the Gross Domestic Product by 2025.</p>

                            <p>To bridge these gaps that exist, India formally adopted Gender Responsive Budgeting (GRB) in 2005. The rationale behind GRB is that policy outcomes are not as gender-neutral as commonly believed, and can reinforce or exacerbate exiting hierarchies. Hence, gender budgeting initiatives aim to integrate critical gender concerns into fiscal policies and administration to address disparities.</p>

                            <p>However to truly empower women the policies have to be designed keeping equal opportunities and equal access in mind. Ensuring a girl child gets the education she deserves, the safety she needs and the career growth opportunities that she works towards is a level playing field is the responsibility of the policy makers. In terms of political space it is time that the women’s reservation bill is taken up on a priority and cleared in both the houses of the parliament. The country saw a huge shift towards women participating in local governments after Late Shri Rajiv Gandhi’s push to amend the constitution to ensure more representation. It is time to take that forward to assembly and parliament level.</p>

                            <p>It is heartening to note that though we haven’t yet reached where we have to we are not hesitating to keep fighting the good fight. The women of our country are not just rising but are also aware of what they want and how to achieve it. It is for the government to rise up to the challenge and take this momentum forward.</p>

                            </p>
                        </div>
                    </div>

                    <div id="author" class="blue">
                        <div class="about-author">
                            <div class="author">
                                <img src="../images/priyanka_chaturvedi.jpeg" alt="author">
                                <h3 class="">Priyanka Chaturvedi</h3>
                                <p>CONVENOR -AICC, COMMUNICATION & NATIONAL SPOKESPERSON</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-5 col-sm-6 right-post-single">
            <div id="sidebar">
                <div class="sidebar blue ">

                    <div class="search">
                        <form class="form-search" action="//google.com.ua/search" target="_blank" method="get" onsubmit="return q.value!=''" role="search">
                            <label>Search</label>
                            <input type="search" name="q" placeholder="Search...">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                 <!--    <div class="recent-posts mb">
                        <h4> Recent Posts </h4>
                        <div class="news-1">
                            <a href="">
                                <img src="images/bridging-gender-gap-in-education.jpg" alt="...">
                            </a>
                            <div class="news">
                                <h5 class="news-h"><a href="https://thx-html.fruitfulcode.com/charity-blog-single">Blog Title</a> </h5>
                                <p class="date">June 28, 2017</p>
                            </div>
                        </div>
                    </div> -->
                    <div class="tags">
                        <h4>Tags</h4>

                        <ul>
                            <li class="active buttons"><a href="https://thx-html.fruitfulcode.com/charity-blog-single" class="tags-btn">Philanthropy</a></li>
                            <li class="buttons"><a href="" class="tags-btn">Disaster</a></li>
                            <li class="buttons"><a href="" class="tags-btn">Marathon</a></li>
                            <li class="buttons"><a href="" class="tags-btn">Refugee</a></li>
                            <li class="buttons"><a href="" class="tags-btn">AIDS </a></li>
                            <li class="buttons"><a href="" class="tags-btn">Red Cross</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@stop