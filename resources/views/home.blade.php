@extends('layout')
@section('title') Home - #WomenFor33 | All India Mahila Congress @stop
@section('page-content')
    <div class="slider">
        <div id="home-carousel" class="carousel" data-ride="carousel">
            <div class="carousel-inner" role="listbox">

                <div class="item slide_1 active">
                    <div class="container p0">
                        <div class="tp-mask-wrap" style="position: absolute; display: block; overflow: visible;"><div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-29-layer-18" data-x="['left','left','left','left']" data-hoffset="['-150','-150','-150','-150']" data-y="['top','top','top','top']" data-voffset="['100','100','100','100']" data-width="420" data-height="full" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="shape" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:2000,&quot;frame&quot;:&quot;0&quot;,&quot;to&quot;:&quot;o:1;sY:3;skX:-30px;&quot;,&quot;ease&quot;:&quot;Linear.easeNone&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:10,&quot;frame&quot;:&quot;999&quot;,&quot;ease&quot;:&quot;nothing&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; background-color: rgba(6, 57, 106, 0.8); visibility: inherit; transition: none; text-align: inherit; line-height: 25px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 16px; white-space: nowrap; min-height: 700px; min-width: 420px; max-width: 420px; opacity: 1; transform: matrix3d(1, 0, 0, 0, -1.5, 2.59808, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 53% 0px;"> </div></div>
                        <div class="item-content">
                            <div class="gb-middle ">
                                <div class="slider-info ">
                                    <h2>#WomenFor33</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item slide_2">
                    <div class="container p0">
                        <div class="tp-mask-wrap" style="position: absolute; display: block; overflow: visible;"><div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-29-layer-18" data-x="['left','left','left','left']" data-hoffset="['-150','-150','-150','-150']" data-y="['top','top','top','top']" data-voffset="['100','100','100','100']" data-width="420" data-height="full" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="shape" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:2000,&quot;frame&quot;:&quot;0&quot;,&quot;to&quot;:&quot;o:1;sY:3;skX:-30px;&quot;,&quot;ease&quot;:&quot;Linear.easeNone&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:10,&quot;frame&quot;:&quot;999&quot;,&quot;ease&quot;:&quot;nothing&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; background-color: rgba(6, 57, 106, 0.8); visibility: inherit; transition: none; text-align: inherit; line-height: 25px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 16px; white-space: nowrap; min-height: 700px; min-width: 420px; max-width: 420px; opacity: 1; transform: matrix3d(1, 0, 0, 0, -1.5, 2.59808, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 53% 0px;"> </div></div>
                        <div class="item-content">
                            <div class="gb-middle ">
                                <div class="slider-info ">
                                    <h2>#WomenFor33</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item slide_3">
                    <div class="container p0">
                        <div class="tp-mask-wrap" style="position: absolute; display: block; overflow: visible;"><div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-29-layer-18" data-x="['left','left','left','left']" data-hoffset="['-150','-150','-150','-150']" data-y="['top','top','top','top']" data-voffset="['100','100','100','100']" data-width="420" data-height="full" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="shape" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:2000,&quot;frame&quot;:&quot;0&quot;,&quot;to&quot;:&quot;o:1;sY:3;skX:-30px;&quot;,&quot;ease&quot;:&quot;Linear.easeNone&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:10,&quot;frame&quot;:&quot;999&quot;,&quot;ease&quot;:&quot;nothing&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; background-color: rgba(6, 57, 106, 0.8); visibility: inherit; transition: none; text-align: inherit; line-height: 25px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 16px; white-space: nowrap; min-height: 700px; min-width: 420px; max-width: 420px; opacity: 1; transform: matrix3d(1, 0, 0, 0, -1.5, 2.59808, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 53% 0px;"> </div></div>
                        <div class="item-content">
                            <div class="gb-middle ">
                                <div class="slider-info ">
                                    <h2>#WomenFor33</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <ol class="carousel-indicators">
                <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
                <li class="two" data-target="#home-carousel" data-slide-to="1"></li>
                <li data-target="#home-carousel" data-slide-to="2" class=""></li>
            </ol>

        </div>
    </div>
    
    <div class="number" id="counts">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 mb center ">
                    <div class="text">
                        <p>
                            <span class="spincrement">3206257</span>
                            <span class="text-1"> Signatures Collected </span>
                        </p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="main-container-1">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <div class="youtube">
                                <div class="youtube-overlay">
                                    <img src="https://thx-html.fruitfulcode.com/wp-content/themes/thx-html/assets/images/btn-youtube.png" alt="btn-youtube">
                                </div>
                                <iframe id="iframeYoutube"
                                        ></iframe>
                            </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="container-text">
                        <h2 class="title-text">#WomenFor33%</h2>

                        <div class="wave-line"></div>

                        <p class="paragraph justify">
                            Our current campaign #WomenFor33% is a landmark endeavor to gather public support for giving women 33% reservation in all elected bodies from Panchayat to Parliament. We believe that once passed this landmark legislation will change the power equations in the country forever.
                        </p>

                        <div class="buttons" style="text-align: right">
                            <a href="#" class=" red-btn red-btn-1">Read More</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="main-container-4">
        <div class="container">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center">
                        <h2 class="title-text" style="color: #fff;">Representation Matters</h2>
                        <div class="wave-line wave-center"></div>
                        
                        <p class="paragraph-white " style="color:#fff;">Gender parity in every field, most importatntly in Political area, is a challenge and the call of the hour, not just in India but world over.</p>

                        <p class="paragraph-white " style="color:#fff;">With an absolute majority,  Prime Minister Sh. Narendra Modi has an immence oppurtunity as well as resposnbility to ensure more women find  voice in legislative bodies, thus truly empowering 48% of India’s population.</p>

                        <p class="paragraph-white " style="color:#fff;">Click on the button below to request the <strong>Prime Minister</strong> to introduced WRB in the Parliament at the earliest and do your bit to empower 8% of humanity.</p>

                        <div class="buttons" style="text-align: center">
                            <a href="#" id="writepm" class=" red-btn red-btn-1">Write to the Prime Minister</a>
                        </div>

                    </div>
                </div>
            </div>

<!--             <div class="row ">
                <div class="col-md-4">
                    <form class="form-features" id="find-mp">
                        <div class="row row-rel">
                            <div class="col-md-12 col-sm-12 col-xs-12 mb-features">
                                <label>Select State</label>
                                <select class="flexselect" name="state" required="">
                                    <option></option>
                                    <option value="1">Andaman and Nicobar Islands</option>
                                    <option value="2">Andhra Pradesh</option>
                                    <option value="3">Arunachal Pradesh</option>
                                    <option value="4">Assam</option>
                                    <option value="5">Bihar</option>
                                    <option value="6">Chandigarh</option>
                                    <option value="7">Chhattisgarh</option>
                                    <option value="8">Dadra and Nagar Haveli</option>
                                    <option value="9">Daman and Diu</option>
                                    <option value="10">Delhi</option>
                                    <option value="11">Goa</option>
                                    <option value="12">Gujarat</option>
                                    <option value="13">Haryana</option>
                                    <option value="14">Himachal Pradesh</option>
                                    <option value="15">Jammu and Kashmir</option>
                                    <option value="16">Jharkhand</option>
                                    <option value="17">Karnataka</option>
                                    <option value="18">Kerala</option>
                                    <option value="19">Lakshadweep</option>
                                    <option value="20">Madhya Pradesh</option>
                                    <option value="21">Maharashtra</option>
                                    <option value="22">Manipur</option>
                                    <option value="23">Meghalaya</option>
                                    <option value="24">Mizoram</option>
                                    <option value="25">Nagaland</option>
                                    <option value="26">Odisha</option>
                                    <option value="27">Puducherry</option>
                                    <option value="28">Punjab</option>
                                    <option value="29">Rajasthan</option>
                                    <option value="30">Sikkim</option>
                                    <option value="31">Tamil Nadu</option>
                                    <option value="32">Telangana</option>
                                    <option value="33">Tripura</option>
                                    <option value="34">Uttar Pradesh</option>
                                    <option value="35">Uttarakhand</option>
                                    <option value="36">West Bengal</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 mb-features">
                                <label>Select Constituency</label>
                                <select class="flexselect" name="constituency" required="">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 mb-features">
                                <input type="submit" value="Find MP">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-8" id="pc_map"></div>
            </div> -->

                <div class="row mp-data">
                <div class="col-lg-12">
                    <form class="form-features" action="...">
                        <div class="row row-rel">
                            <div class="col-md-3 col-sm-3 col-xs-12 mb-features">
                                <label>Name</label>
                                <input type="text" placeholder="" class="mp_name" disabled>
                            </div><div class="col-md-3 col-sm-3 col-xs-12 mb-features">
                                <label>Party</label>
                                <input type="text" placeholder="" class="mp_party" disabled>
                            </div>   
                            <div class="col-md-3 col-sm-3 col-xs-12 mb-features">
                                <label>Opinion</label>
                                <input type="text" placeholder="" class="mp_opinion" disabled>
                            </div> 
                           
                            <div class="col-md-3 col-sm-3 col-xs-12 col-abs mb-features">
                                <input type="button" value="Write To MP" id="writemp"">
                            </div>   
                        </div>
                    </form>
                </div>
            </div>
            </div>
            </div>























    
<!--     <div class="main-container-4">
        <div class="container">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center">
                        <h2 class="title-text">Ensure Your Member of Parliament's Support</h2>
                        <div class="wave-line wave-center"></div>
                      
                        <p class="paragraph-white ">Find out What is your MP's Response. Help Our Initiative By Asking Your MP</p>
                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-md-4">
                    <form class="form-features" id="find-mp">
                        <div class="row row-rel">
                            <div class="col-md-12 col-sm-12 col-xs-12 mb-features">
                                <label>Select State</label>
                                <select class="flexselect" name="state" required="">
                                    <option></option>
                                    <option value="1">Andaman and Nicobar Islands</option>
                                    <option value="2">Andhra Pradesh</option>
                                    <option value="3">Arunachal Pradesh</option>
                                    <option value="4">Assam</option>
                                    <option value="5">Bihar</option>
                                    <option value="6">Chandigarh</option>
                                    <option value="7">Chhattisgarh</option>
                                    <option value="8">Dadra and Nagar Haveli</option>
                                    <option value="9">Daman and Diu</option>
                                    <option value="10">Delhi</option>
                                    <option value="11">Goa</option>
                                    <option value="12">Gujarat</option>
                                    <option value="13">Haryana</option>
                                    <option value="14">Himachal Pradesh</option>
                                    <option value="15">Jammu and Kashmir</option>
                                    <option value="16">Jharkhand</option>
                                    <option value="17">Karnataka</option>
                                    <option value="18">Kerala</option>
                                    <option value="19">Lakshadweep</option>
                                    <option value="20">Madhya Pradesh</option>
                                    <option value="21">Maharashtra</option>
                                    <option value="22">Manipur</option>
                                    <option value="23">Meghalaya</option>
                                    <option value="24">Mizoram</option>
                                    <option value="25">Nagaland</option>
                                    <option value="26">Odisha</option>
                                    <option value="27">Puducherry</option>
                                    <option value="28">Punjab</option>
                                    <option value="29">Rajasthan</option>
                                    <option value="30">Sikkim</option>
                                    <option value="31">Tamil Nadu</option>
                                    <option value="32">Telangana</option>
                                    <option value="33">Tripura</option>
                                    <option value="34">Uttar Pradesh</option>
                                    <option value="35">Uttarakhand</option>
                                    <option value="36">West Bengal</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 mb-features">
                                <label>Select Constituency</label>
                                <select class="flexselect" name="constituency" required="">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 mb-features">
                                <input type="submit" value="Find MP">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-8" id="pc_map"></div>
            </div>

                <div class="row mp-data">
                <div class="col-lg-12">
                    <form class="form-features" action="...">
                        <div class="row row-rel">
                            <div class="col-md-3 col-sm-3 col-xs-12 mb-features">
                                <label>Name</label>
                                <input type="text" placeholder="" class="mp_name" disabled>
                            </div><div class="col-md-3 col-sm-3 col-xs-12 mb-features">
                                <label>Party</label>
                                <input type="text" placeholder="" class="mp_party" disabled>
                            </div>   
                            <div class="col-md-3 col-sm-3 col-xs-12 mb-features">
                                <label>Opinion</label>
                                <input type="text" placeholder="" class="mp_opinion" disabled>
                            </div> 
                           
                            <div class="col-md-3 col-sm-3 col-xs-12 col-abs mb-features">
                                <input type="button" value="Write To MP" id="writemp"">
                            </div>   
                        </div>
                    </form>
                </div>
            </div>
            </div>
            </div> -->

    </div>

    <div class="main-container-7">
        <div class="container">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center">
                        <h2 class="title-text">Blog #WomenFor33</h2>
                        <div class="wave-line wave-center"></div>
                        
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-6 mb ">
                    <div class="person-card">
                        <div class="person-img">
                            <a href="/blog/bridging-gender-gap-in-education-through-ict" class="img-link">
                                <span><i class="icon-eye" aria-hidden="true"></i></span>
                                <img src="images/bridging-gender-gap-in-education.jpg" alt="">
                            </a>
                        </div>
                        <div class="person-content">
                            <h3><a href="/blog/bridging-gender-gap-in-education-through-ict" class="title-white">Bridging Gender Gap in Education Through ICT</a></h3>
                            <ul class="gb-list">
                                <li>
                                    <a href="/blog/bridging-gender-gap-in-education-through-ict"> Dec 12, 2017 </a>
                                </li>
                            </ul>
                            <p>Gender disparity is a burning issue in India. Indian Constitution not only guarantees gender equality, but also directs the State to take positive action in this direction ...</p>
                        </div>
                    </div>
                    <div class="buttons">
                        <a href="/blog/bridging-gender-gap-in-education-through-ict" class="red-btn red-btn-7">Read More</a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 mb ">
                    <div class="person-card">
                        <div class="person-img">
                            <a href="/blog/stride-towards-equality" class="img-link">
                                <span><i class="icon-eye" aria-hidden="true"></i></span>
                                <img src="images/blog2.jpg" alt="">
                            </a>
                        </div>
                        <div class="person-content">
                            <h3><a href="/blog/stride-towards-equality" class="title-white">Stride Towards Equality</a></h3>
                            <ul class="gb-list">
                                <li>
                                    <a href="/blog/stride-towards-equality"> Dec 12, 2017 </a>
                                </li>
                            </ul>
                            <p>The women across the world have struggled over the years to be able to get equal opportunities and access for themselves.Who can forget the suffragette movement seeking voting rights ...</p>
                        </div>
                    </div>
                    <div class="buttons">
                        <a href="/blog/stride-towards-equality" class="red-btn red-btn-7">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

        <div class="events black">
        <div class="main-container-3 shadow">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                        <h2 class="title-text">Bill Timeline</h2>
                        <div class="wave-line wave-center"></div>
                        <section class="cd-horizontal-timeline">
    <div class="timeline">
        <div class="events-wrapper">
            <div class="events">
                <ol>
                    <li><a href="#0" data-date="16/01/2014" class="selected">1980</a></li>
                    <li><a href="#0" data-date="16/02/2014">1992</a></li>
                    <li><a href="#0" data-date="16/03/2014">2008</a></li>
                    <li><a href="#0" data-date="16/04/2014">2009</a></li>
                    <li><a href="#0" data-date="16/05/2014">2010</a></li>
                    <li><a href="#0" data-date="16/06/2014">2014</a></li>
                    <li><a href="#0" data-date="16/07/2014">2017</a></li>
                    <li><a href="#0" data-date="16/08/2014">2017</a></li>
                    <li><a href="#0" data-date="16/09/2014">2017</a></li>
                    <li><a href="#0" data-date="16/10/2014">2017</a></li>
                    <li><a href="#0" data-date="16/11/2014">2017</a></li>
                    <li><a href="#0" data-date="16/12/2014">2017</a></li>
                    <li><a href="#0" data-date="16/1/2015">2017</a></li>

                </ol>

                <span class="filling-line" aria-hidden="true"></span>
            </div> 
        </div> 
            
        <ul class="cd-timeline-navigation">
            <li><a href="#0" class="prev inactive"><</a></li>
            <li><a href="#0" class="next">></a></li>
        </ul> 
    </div> 

    <div class="events-content">
        <ol>
            <li class="selected" data-date="16/01/2014">
                <p>Shri. Rajiv Gandhi ji’s vision of Politically empowering Women initiated the movement for Women’s reservation in all elected bodies.</p>
                 <em>1980</em>
            </li>
            <li data-date="16/02/2014">
                <p>(73rd Amendment) Bill, sought to have 1/3rd seats, out of the total number of seats, to be filled by the direct elections to be reserved for women</p>
                <em>1992</em>
            </li>
            <li data-date="16/03/2014">
                <p>The Women's Reservation Bill (WRB) proposed to amend the Constitution of India to reserve 33% of all seats in LokSabha, and in all state legislative assemblies for women.</p>
                <em>2008</em>
            </li>
            <li data-date="16/04/2014">
                <p>(One 110th Amendment) Bill, was introduced in the Lok Sabha to amend the Article 243 D to enhance the proportion of reservation for women from one-third to one-half of the total seats in the Panchayats.</p>
                <em>2009</em>
            </li>
            <li data-date="16/05/2014">
                <p>Women’s Reservation Bill was placed in Parliament and 14 years after its introduction, it was finally passed by the Rajya Sabha under the aegis of Smt. Sonia Gandhi ji.</p>
                <em>April, 2010</em>
            </li>
            <li data-date="16/06/2014">
                <p>The Lok Sabha never voted on the Bill and the Bill lapsed after the dissolution of the 15th Lok Sabha.</p>
                <em>2014</em>
            </li>
            <li data-date="16/07/2014">
                <p>Balidaan Diwas, Signature Campaign launched.</p>
                <em>21 May, 2017</em>
            </li>
            <li data-date="16/08/2014">
                <img src="images/letter-mos_092117015624.jpg" width="20%" style="margin: 10px 0px" />
                <br/>
                <p>Smt. Sonia Gandhi ji writes to PM asking him to bring WRB in upcoming Parliament Session.</p>

                <em>20 September, 2017</em>
            </li>
            <li data-date="16/09/2014">
                <p>32 Lac+ signatures collected Country wide under the Signature Campaign.</p>
                <em>21 September, 2017</em>
            </li>
            <li data-date="16/10/2014">
                <p>AIMC President led a delegation of MC team and meets Hon'ble President of India with a Petition requesting his supprt for #WomenFor33% to be tabled and passed in Parliament in the next session.</p>
                <em>23 October, 2017</em>
            </li>
            <li data-date="16/11/2014">
                <p>MC delegations submitted a demand to the Prime Minister with local DC asking PM to pass the Women’s Reservation Bill.</p>
                <em>31 October, 2017</em>
            </li>
            <li data-date="16/12/2014">
                <p>All Pradesh Mahila Congress delegations led by their Presidents wrote to their Chief Ministers, requesting his support in urging Prime Minister Narendra Modi ji to introduce and pass the WRB in upcoming Session of Parliament.</p>
                <em>19 November, 2017</em>
            </li>
            <li data-date="16/1/2015">
                <p>All Pradesh Mahila Cpngress delegations led by their Presidents Protested against the State Chief Ministers who did not grant them an audience for meeting regarding WRB in November.</p>
                <em>9 December, 2017</em>
            </li>
        </ol>
    </div> 
</section>
                       
                    </div>
                </div>


            </div>
        </div>
    </div>



    <div class="photo-container">
        <div class="container-fluid photo">
            <div class="photo-link">
                <a href="#" class="img-link">
                    <img src="images/wf33_g1.jpg" alt="" class="img-small">
                </a>
            </div>
            <div class="photo-link">
                <a href="#" class="img-link">
                    <img src="images/wf33_g2.jpg" alt="" class="img-small">
                </a>
            </div>
            <div class="photo-link">
                <a href="#" class="img-link">
                    <img src="images/wf33_g3.jpg" alt="" class="img-small">
                </a>
            </div>
             <div class="photo-link">
                <a href="#" class="img-link">
                    <img src="images/wf33_g6.jpg" alt="" class="img-small">
                </a>
            </div>
            <div class="photo-link">
                <a href="#" class="img-link">
                    <img src="images/wf33_g4.jpg" alt="" class="img-small">
                </a>
            </div>
            <div class="photo-link">
                <a href="#" class="img-link">
                    <img src="images/wf33_g5.jpg" alt="" class="img-small">
                </a>
            </div>
            <div class="photo-link">
                <a href="#" class="img-link">
                    <img src="images/wf33_g7.jpg" alt="" class="img-small">
                </a>
            </div>
            <div class="photo-link">
                <a href="#" class="img-link">
                    <img src="images/wf33_g8.jpg" alt="" class="img-small">
                </a>
            </div>
        </div>
    </div>

<div id="sendpm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Request for introduction and implementation of Women’s Reservation Bill</h4>
      </div>
      <div class="modal-body">
        <form role="form" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2" for="inputTo">Name</label>
                  <div class="col-sm-10"><input type="text" class="form-control" name="sender_name" style="background-color: #eee"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputTo">Email</label>
                  <div class="col-sm-10"><input type="text" class="form-control" name="sender_email" style="background-color: #eee"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputTo">Number</label>
                  <div class="col-sm-10"><input type="text" class="form-control" name="sender_number" style="background-color: #eee"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputTo">To</label>
                  <div class="col-sm-10"><input type="text" class="form-control" value="pmh7rcr@gov.in , connect@mygov.nic.in" name="pm_mail" disabled></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Subject</label>
                  <div class="col-sm-10"><input type="text" name="inputSubject" class="form-control" value="Request for introduction and implementation of Women’s Reservation Bill" disabled></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12" for="inputBody">Message</label>
                  <div class="col-sm-12"><textarea disabled class="form-control" id="inputBody" rows="12" cols="12" style="color:#000 !important; text-align:justify;">Respected Prime Minister Sir,&#13;&#10;&#13;&#10;I am writing to you today as a citizen of this great nation on a matter that concerns all of us deeply.&#13;&#10;&#13;&#10;Women make up half of India’s population and have made enormous progress in various walks of life, but they still remain poorly represented in policy making and legislative bodies, standing at mere 11% representation in Parliament today.&#13;&#10;&#13;&#10;As India moves ahead on its journey of becoming one of the leading nations in the world, there will be many issues that Parliament or Legislative Assemblies will vote on that will disproportionately affect women: labour rights, minimum wages, security and privacy, just to name a few. When these issues come up, the debates in the Legislative Houses will be best served if there are more female voices that will bring their own unique experiences and perspectives to ensure that we have more robust debates that result in even more balanced legislative outcomes.&#13;&#10;&#13;&#10;Our nation’s commitment to women’s equality will be judged on the basis of our action on this crucial Bill that will bring 33% reservation for women in Parliament and State Assemblies.&#13;&#10;&#13;&#10;I urge you to use the powers vested by our Constitution in your office and introduce this Bill and thereby making true political empowerment of women a reality in this great democracy of ours!&#13;&#10;&#13;&#10;Thank You.
                  </textarea></div>
                </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="send-mail-pm">Send Mail</button>
        <h4 style="padding: 20px 0px" class="reload-message">Reloading Page, Please Wait!</h4>
        <h6 style="padding: 20px 10px; text-align: left; margin: 10px 0px;" class="alert alert-danger error-message">Please Enter Your Name</h6>
      </div>
    </div>

  </div>
</div>

@stop
@section('page-scripts')
<script type="text/javascript">
$spinner = "<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>";

$(function() {
    // $("select[name='state']").change(function(){
    //     $("select[name='constituency']").empty();
    //     $(".mp-data").hide();
    //     var id = $(this).find(":selected").val();
    //     $.ajax({
    //         type: "get",
    //         url: "/getconstituency/"+id,
    //         success: function(data){
    //             $( "select[name='constituency']").append("<option>Select Constituency</option>");
    //             $.each( data, function( key, value ) {
    //                 $( "select[name='constituency']").append("<option value="+value+">"+key+"</option>");
    //             });
    //             $("select[name='constituency']").flexselect();
    //         }
    //     });
    // });
    // $("select[name='constituency']").change(function(){
    //     $(".mp-data").hide();
    // });
    // $("form#find-mp").submit(function(e) {
    //     e.preventDefault();
    //     var el = $("select[name='constituency']");
    //     var id = el.find(":selected").val();
    //     $.ajax({
    //         type: "get",
    //         url: "/getopinion/"+id,
    //         success: function(data){
    //             mp_id = data.id;
    //             mp_name = data.name;
    //             mp_email = data.email;
    //             mp_party = data.party;
    //             mp_opinion = data.opinion;
    //             if(mp_opinion === "null")
    //             {
    //                 mp_opinion = "No Comment Yet";
    //             }
    //             $(".mp_name").val(mp_name);
    //             $(".mp_party").val(mp_party);
    //             $(".mp_opinion").val(mp_opinion);
    //             $(".mp-data").show();
    //         }        
    //     });
    // });

    $("#writepm").click(function(event) {
        event.preventDefault();
        $("#sendpm").modal("show");
    });

    $("#send-mail-pm").click(function(event) {
        event.preventDefault();
        $ele = $(this);
        $name = $("input[name=sender_name]").val();
        if($name){
            $(".error-message").hide();
            $ele.html($spinner);
            $number = $("input[name=sender_number]").val();
            if(!$number){
                $number = 'number'; 
            }

            $email = $("input[name=sender_email]").val();
            if(!$email){
                $email = 'email'; 
            }
            $.ajax({
            type: "get",
            url: "/sendMailPM/"+$name+"/"+$email+"/"+$number,
            success: function(data){
                if(data=="success"){
                    $ele.html("Mail Sent");
                    $(".reload-message").show();
                    window.setTimeout(function(){location.reload()},2000);    
                }
            }        
            });
        }
        else{
            $(".error-message").show();
        }
    });

    /*Timeline*/

    var timelines = $('.cd-horizontal-timeline'),
        eventsMinDistance = 60;

    (timelines.length > 0) && initTimeline(timelines);

    function initTimeline(timelines) {
        timelines.each(function(){
            var timeline = $(this),
                timelineComponents = {};
            //cache timeline components 
            timelineComponents['timelineWrapper'] = timeline.find('.events-wrapper');
            timelineComponents['eventsWrapper'] = timelineComponents['timelineWrapper'].children('.events');
            timelineComponents['fillingLine'] = timelineComponents['eventsWrapper'].children('.filling-line');
            timelineComponents['timelineEvents'] = timelineComponents['eventsWrapper'].find('a');
            timelineComponents['timelineDates'] = parseDate(timelineComponents['timelineEvents']);
            timelineComponents['eventsMinLapse'] = minLapse(timelineComponents['timelineDates']);
            timelineComponents['timelineNavigation'] = timeline.find('.cd-timeline-navigation');
            timelineComponents['eventsContent'] = timeline.children('.events-content');

            //assign a left postion to the single events along the timeline
            setDatePosition(timelineComponents, eventsMinDistance);
            //assign a width to the timeline
            var timelineTotWidth = setTimelineWidth(timelineComponents, eventsMinDistance);
            //the timeline has been initialize - show it
            timeline.addClass('loaded');

            //detect click on the next arrow
            timelineComponents['timelineNavigation'].on('click', '.next', function(event){
                event.preventDefault();
                updateSlide(timelineComponents, timelineTotWidth, 'next');
            });
            //detect click on the prev arrow
            timelineComponents['timelineNavigation'].on('click', '.prev', function(event){
                event.preventDefault();
                updateSlide(timelineComponents, timelineTotWidth, 'prev');
            });
            //detect click on the a single event - show new event content
            timelineComponents['eventsWrapper'].on('click', 'a', function(event){
                event.preventDefault();
                timelineComponents['timelineEvents'].removeClass('selected');
                $(this).addClass('selected');
                updateOlderEvents($(this));
                updateFilling($(this), timelineComponents['fillingLine'], timelineTotWidth);
                updateVisibleContent($(this), timelineComponents['eventsContent']);
            });

            //on swipe, show next/prev event content
            timelineComponents['eventsContent'].on('swipeleft', function(){
                var mq = checkMQ();
                ( mq == 'mobile' ) && showNewContent(timelineComponents, timelineTotWidth, 'next');
            });
            timelineComponents['eventsContent'].on('swiperight', function(){
                var mq = checkMQ();
                ( mq == 'mobile' ) && showNewContent(timelineComponents, timelineTotWidth, 'prev');
            });

            //keyboard navigation
            $(document).keyup(function(event){
                if(event.which=='37' && elementInViewport(timeline.get(0)) ) {
                    showNewContent(timelineComponents, timelineTotWidth, 'prev');
                } else if( event.which=='39' && elementInViewport(timeline.get(0))) {
                    showNewContent(timelineComponents, timelineTotWidth, 'next');
                }
            });
        });
    }

    function updateSlide(timelineComponents, timelineTotWidth, string) {
        //retrieve translateX value of timelineComponents['eventsWrapper']
        var translateValue = getTranslateValue(timelineComponents['eventsWrapper']),
            wrapperWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', ''));
        //translate the timeline to the left('next')/right('prev') 
        (string == 'next') 
            ? translateTimeline(timelineComponents, translateValue - wrapperWidth + eventsMinDistance, wrapperWidth - timelineTotWidth)
            : translateTimeline(timelineComponents, translateValue + wrapperWidth - eventsMinDistance);
    }

    function showNewContent(timelineComponents, timelineTotWidth, string) {
        //go from one event to the next/previous one
        var visibleContent =  timelineComponents['eventsContent'].find('.selected'),
            newContent = ( string == 'next' ) ? visibleContent.next() : visibleContent.prev();

        if ( newContent.length > 0 ) { //if there's a next/prev event - show it
            var selectedDate = timelineComponents['eventsWrapper'].find('.selected'),
                newEvent = ( string == 'next' ) ? selectedDate.parent('li').next('li').children('a') : selectedDate.parent('li').prev('li').children('a');
            
            updateFilling(newEvent, timelineComponents['fillingLine'], timelineTotWidth);
            updateVisibleContent(newEvent, timelineComponents['eventsContent']);
            newEvent.addClass('selected');
            selectedDate.removeClass('selected');
            updateOlderEvents(newEvent);
            updateTimelinePosition(string, newEvent, timelineComponents, timelineTotWidth);
        }
    }

    function updateTimelinePosition(string, event, timelineComponents, timelineTotWidth) {
        //translate timeline to the left/right according to the position of the selected event
        var eventStyle = window.getComputedStyle(event.get(0), null),
            eventLeft = Number(eventStyle.getPropertyValue("left").replace('px', '')),
            timelineWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', '')),
            timelineTotWidth = Number(timelineComponents['eventsWrapper'].css('width').replace('px', ''));
        var timelineTranslate = getTranslateValue(timelineComponents['eventsWrapper']);

        if( (string == 'next' && eventLeft > timelineWidth - timelineTranslate) || (string == 'prev' && eventLeft < - timelineTranslate) ) {
            translateTimeline(timelineComponents, - eventLeft + timelineWidth/2, timelineWidth - timelineTotWidth);
        }
    }

    function translateTimeline(timelineComponents, value, totWidth) {
        var eventsWrapper = timelineComponents['eventsWrapper'].get(0);
        value = (value > 0) ? 0 : value; //only negative translate value
        value = ( !(typeof totWidth === 'undefined') &&  value < totWidth ) ? totWidth : value; //do not translate more than timeline width
        setTransformValue(eventsWrapper, 'translateX', value+'px');
        //update navigation arrows visibility
        (value == 0 ) ? timelineComponents['timelineNavigation'].find('.prev').addClass('inactive') : timelineComponents['timelineNavigation'].find('.prev').removeClass('inactive');
        (value == totWidth ) ? timelineComponents['timelineNavigation'].find('.next').addClass('inactive') : timelineComponents['timelineNavigation'].find('.next').removeClass('inactive');
    }

    function updateFilling(selectedEvent, filling, totWidth) {
        //change .filling-line length according to the selected event
        var eventStyle = window.getComputedStyle(selectedEvent.get(0), null),
            eventLeft = eventStyle.getPropertyValue("left"),
            eventWidth = eventStyle.getPropertyValue("width");
        eventLeft = Number(eventLeft.replace('px', '')) + Number(eventWidth.replace('px', ''))/2;
        var scaleValue = eventLeft/totWidth;
        setTransformValue(filling.get(0), 'scaleX', scaleValue);
    }

    function setDatePosition(timelineComponents, min) {
        for (i = 0; i < timelineComponents['timelineDates'].length; i++) { 
            var distance = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][i]),
                distanceNorm = Math.round(distance/timelineComponents['eventsMinLapse']) + 2;
            timelineComponents['timelineEvents'].eq(i).css('left', distanceNorm*min+'px');
        }
    }

    function setTimelineWidth(timelineComponents, width) {
        var timeSpan = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][timelineComponents['timelineDates'].length-1]),
            timeSpanNorm = timeSpan/timelineComponents['eventsMinLapse'],
            timeSpanNorm = Math.round(timeSpanNorm) + 4,
            totalWidth = timeSpanNorm*width;
        timelineComponents['eventsWrapper'].css('width', totalWidth+'px');
        updateFilling(timelineComponents['timelineEvents'].eq(0), timelineComponents['fillingLine'], totalWidth);
    
        return totalWidth;
    }

    function updateVisibleContent(event, eventsContent) {
        var eventDate = event.data('date'),
            visibleContent = eventsContent.find('.selected'),
            selectedContent = eventsContent.find('[data-date="'+ eventDate +'"]'),
            selectedContentHeight = selectedContent.height();

        if (selectedContent.index() > visibleContent.index()) {
            var classEnetering = 'selected enter-right',
                classLeaving = 'leave-left';
        } else {
            var classEnetering = 'selected enter-left',
                classLeaving = 'leave-right';
        }

        selectedContent.attr('class', classEnetering);
        visibleContent.attr('class', classLeaving).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
            visibleContent.removeClass('leave-right leave-left');
            selectedContent.removeClass('enter-left enter-right');
        });
        eventsContent.css('height', selectedContentHeight+'px');
    }

    function updateOlderEvents(event) {
        event.parent('li').prevAll('li').children('a').addClass('older-event').end().end().nextAll('li').children('a').removeClass('older-event');
    }

    function getTranslateValue(timeline) {
        var timelineStyle = window.getComputedStyle(timeline.get(0), null),
            timelineTranslate = timelineStyle.getPropertyValue("-webkit-transform") ||
                timelineStyle.getPropertyValue("-moz-transform") ||
                timelineStyle.getPropertyValue("-ms-transform") ||
                timelineStyle.getPropertyValue("-o-transform") ||
                timelineStyle.getPropertyValue("transform");

        if( timelineTranslate.indexOf('(') >=0 ) {
            var timelineTranslate = timelineTranslate.split('(')[1];
            timelineTranslate = timelineTranslate.split(')')[0];
            timelineTranslate = timelineTranslate.split(',');
            var translateValue = timelineTranslate[4];
        } else {
            var translateValue = 0;
        }

        return Number(translateValue);
    }

    function setTransformValue(element, property, value) {
        element.style["-webkit-transform"] = property+"("+value+")";
        element.style["-moz-transform"] = property+"("+value+")";
        element.style["-ms-transform"] = property+"("+value+")";
        element.style["-o-transform"] = property+"("+value+")";
        element.style["transform"] = property+"("+value+")";
    }

    function parseDate(events) {
        var dateArrays = [];
        events.each(function(){
            var dateComp = $(this).data('date').split('/'),
                newDate = new Date(dateComp[2], dateComp[1]-1, dateComp[0]);
            dateArrays.push(newDate);
        });
        return dateArrays;
    }

    function parseDate2(events) {
        var dateArrays = [];
        events.each(function(){
            var singleDate = $(this),
                dateComp = singleDate.data('date').split('T');
            if( dateComp.length > 1 ) { //both DD/MM/YEAR and time are provided
                var dayComp = dateComp[0].split('/'),
                    timeComp = dateComp[1].split(':');
            } else if( dateComp[0].indexOf(':') >=0 ) { //only time is provide
                var dayComp = ["2000", "0", "0"],
                    timeComp = dateComp[0].split(':');
            } else { //only DD/MM/YEAR
                var dayComp = dateComp[0].split('/'),
                    timeComp = ["0", "0"];
            }
            var newDate = new Date(dayComp[2], dayComp[1]-1, dayComp[0], timeComp[0], timeComp[1]);
            dateArrays.push(newDate);
        });
        return dateArrays;
    }

    function daydiff(first, second) {
        return Math.round((second-first));
    }

    function minLapse(dates) {
        //determine the minimum distance among events
        var dateDistances = [];
        for (i = 1; i < dates.length; i++) { 
            var distance = daydiff(dates[i-1], dates[i]);
            dateDistances.push(distance);
        }
        return Math.min.apply(null, dateDistances);
    }

    function elementInViewport(el) {
        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;

        while(el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        return (
            top < (window.pageYOffset + window.innerHeight) &&
            left < (window.pageXOffset + window.innerWidth) &&
            (top + height) > window.pageYOffset &&
            (left + width) > window.pageXOffset
        );
    }

    function checkMQ() {
        //check if mobile or desktop device
        return window.getComputedStyle(document.querySelector('.cd-horizontal-timeline'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
    }
});
</script>

@stop