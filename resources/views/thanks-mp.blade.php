@extends('layout')
@section('title') Thanks - #WomenFor33 | All India Mahila Congress @stop
@section('page-content')
<div class="banner">
    <div class="shadow-main">
        <h1> Thanks </h1>
    </div>
</div>
    <div class="main-contact">
        <div class="container" style="text-align: center">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
             		<h2>Thanks For Your Opinion</h2>
                </div>
                <div class="buttons">
                    <a href="/" class=" red-btn red-btn-1">Go To Website</a>
                </div>
            </div>
        </div>
    </div>
@stop