<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PageController@home');
Route::get('/thanks','PageController@thanks');
Route::get('/thanks-mp','PageController@thanksmp');
Route::get('/blog/bridging-gender-gap-in-education-through-ict','PageController@blog2');
Route::get('/blog/stride-towards-equality','PageController@blog1');
Route::get('/give-your-support','PageController@giveYourSupport');
Route::get('/sendMailAll','PoliticianController@sendMailAll');
Route::get('/sendMailMP/{id}','PoliticianController@sendMailMP');
Route::get('/sendMailPM/{name}/{email}/{number}','PMMailController@sendMailPM');

Route::get('/setopinion/{token}/{opinion}','PoliticianController@setOpinion');
Route::get('/getopinion/{id}','PoliticianController@getOpinion');
Route::get('/getconstituency/{id}','PoliticianController@getConstituency');
Route::resource('politician','PoliticianController');
Route::resource('user','UserController');

