<?php

use Illuminate\Database\Seeder;

class PoliticiansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('politicians')->insert([
            [
            'state_id' => 1,
            'name' => 'Ashish',
            'party' => 'INC',
            'constituency'=> "Ashish's Constituency",
            'email' => 'sharma.asmith7@gmail.com',
            'token' => str_random(30)
            ],
            [
            'state_id' => 2,
            'name' => 'Ankit',
            'party' => 'INC',
            'constituency'=> "Ankit's Constituency",
            'email' => '0987ankit@gmail.com',
            'token' => str_random(30)
            ],
            [
            'state_id' => 4,
            'name' => 'Km. Sushmita Dev',
            'party' => 'INC',
            'constituency'=>'Silchar',
            'email' => 'sushmita.dev@sansad.nic.in',
            'token' => str_random(30)
            ]
        ]);
    }
}
